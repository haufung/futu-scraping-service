from django.apps import AppConfig


class FutuStockQuoteUpdaterConfig(AppConfig):
    name = 'futuStockQuoteUpdater'

    def ready(self):
        from . import scheduler
        scheduler.start()
