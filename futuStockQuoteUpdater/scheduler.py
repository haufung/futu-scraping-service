from apscheduler.schedulers.background import BackgroundScheduler

from futuStockQuoteUpdater import futuOpenApi


def start():
    scheduler = BackgroundScheduler()
    scheduler.configure(timezone="Asia/Hong_Kong")
    scheduler.add_job(futuOpenApi.callFutuApi, 'interval', seconds=1)
    # scheduler.start()
