import traceback

from django.http import JsonResponse

from .controller import scrapFutuController, patchFutuController, futuStockQuoteController


# Create your views here.
def healthCheck(request):
    return JsonResponse({"code": 200, "message": "UP"})

def scrapCommentInfinite(request):
    try:
        # brand = request.GET['brand']
        data = scrapFutuController.scrapCommentInfinite()
    except Exception as e:
        print(traceback.format_exc())
        return JsonResponse({"code": 500, "status": "INTERNAL SERVER ERROR", "message": e.args[0]}, status=500)
    return JsonResponse({"code": 200, "status": "SUCCESS", "message": "call successful", "data": data})

def patchCommentSentiment(request):
    try:
        # brand = request.GET['brand']
        data = patchFutuController.patchCommentSentiment()
    except Exception as e:
        print(traceback.format_exc())
        return JsonResponse({"code": 500, "status": "INTERNAL SERVER ERROR", "message": e.args[0]}, status=500)
    return JsonResponse({"code": 200, "status": "SUCCESS", "message": "call successful", "data": data})

def patchCommentSentimentSnowNlp(request):
    try:
        data = patchFutuController.patchCommentSentimentSnowNlp()
    except Exception as e:
        print(traceback.format_exc())
        return JsonResponse({"code": 500, "status": "INTERNAL SERVER ERROR", "message": e.args[0]}, status=500)
    return JsonResponse({"code": 200, "status": "SUCCESS", "message": "call successful", "data": data})


def futuStockQuote(request):
    try:
        # brand = request.GET['brand']
        data = futuStockQuoteController.futuStockQuote()
    except Exception as e:
        print(traceback.format_exc())
        return JsonResponse({"code": 500, "status": "INTERNAL SERVER ERROR", "message": e.args[0]}, status=500)
    return JsonResponse({"code": 200, "status": "SUCCESS", "message": "call successful", "data": data})