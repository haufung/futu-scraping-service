from django.urls import path

from . import views

urlpatterns = [
    path('health/', views.healthCheck, name='health'),
    path('scrapCommentInfinite/', views.scrapCommentInfinite, name='scrapCommentInfinite'),
    path('patchCommentSentiment/', views.patchCommentSentiment, name='patchCommentSentiment'),
    path('patchCommentSentimentSnowNlp/', views.patchCommentSentimentSnowNlp, name='patchCommentSentimentSnowNlp'),
    path('futuStockQuote/', views.futuStockQuote, name='futuStockQuote'),
]
