from bs4 import BeautifulSoup
from django.db import connection
from selenium import webdriver
from selenium.webdriver import Keys

from scrapFutu.utils import webDriverUtil, seleniumUtil, snowNlpUtil

cur = connection.cursor()


def checkIfUniqueIdExist(thisUniqueId):
    cur.execute('select count(unique_id) from community_comments where unique_id = %s', [thisUniqueId])
    return cur.fetchall()[0][0] > 0


# deprecated because of cost cutting - 10 Dec 2021
def insertAwsComprehendToDb(fid, uid, userName, stockName, stockUrl, commentText, fullCommentInText, uniqueId,
                            sentiment, sentimentScorePositive, sentimentScoreNegative, sentimentScoreNeutral,
                            sentimentScoreMixed):
    cur.execute(
        'insert into community_comments('
        '"created_datetime_hour_timezone",'
        '"created_datetime_timezone",'
        '"fid",'
        '"uid",'
        '"user_name",'
        '"stock_name",'
        '"stock_url",'
        '"comment",'
        '"full_comment_in_text",'
        '"unique_id",'
        '"sentiment",'
        '"sentiment_score_positive",'
        '"sentiment_score_negative",'
        '"sentiment_score_neutral",'
        '"sentiment_score_mixed")'
        'VALUES ('
        "DATEADD(hh,DATEDIFF(HH, 0, DATEADD(hh,8,CURRENT_TIMESTAMP)),0) AT TIME ZONE 'China Standard Time',"
        "DATEADD(hh,8,CURRENT_TIMESTAMP) AT TIME ZONE 'China Standard Time',"
        '%s,'
        '%s,'
        '%s,'
        '%s,'
        '%s,'
        '%s,'
        '%s,'
        '%s,'
        '%s,'
        '%s,'
        '%s,'
        '%s,'
        '%s)',
        [fid, uid, userName, stockName, stockUrl, commentText, fullCommentInText, uniqueId,
         sentiment, sentimentScorePositive, sentimentScoreNegative, sentimentScoreNeutral,
         sentimentScoreMixed])


def insertSnowNlpToDb(fid, uid, userName, stockName, stockUrl, commentText, fullCommentInText, uniqueId,
                      sentimentSnowNlp):
    cur.execute(
        'insert into community_comments('
        '"created_datetime_hour_timezone",'
        '"created_datetime_timezone",'
        '"fid",'
        '"uid",'
        '"user_name",'
        '"stock_name",'
        '"stock_url",'
        '"comment",'
        '"full_comment_in_text",'
        '"unique_id",'
        '"sentiment_snownlp")'
        'VALUES ('
        "DATEADD(hh,DATEDIFF(HH, 0, DATEADD(hh,8,CURRENT_TIMESTAMP)),0) AT TIME ZONE 'China Standard Time',"
        "DATEADD(hh,8,CURRENT_TIMESTAMP) AT TIME ZONE 'China Standard Time',"
        '%s,'
        '%s,'
        '%s,'
        '%s,'
        '%s,'
        '%s,'
        '%s,'
        '%s,'
        '%s)',
        [fid, uid, userName, stockName, stockUrl, commentText, fullCommentInText, uniqueId,
         sentimentSnowNlp])


def scrapListOfSoupComment(listOfComments):
    needToScrollDown = False
    for index, comment in reversed(list(enumerate(listOfComments))):
        stockName, stockUrl, commentText, fullCommentInText, sentimentSnowNlp = None, None, None, None, None
        fid = comment['fid']
        uid = comment['uid']
        uniqueId = fid + "-" + uid
        if checkIfUniqueIdExist(uniqueId) is not True:
            userName = comment.select('a')[1].getText()
            thisDiv = comment.find_all("div", {"fid": fid})
            if len(thisDiv) > 0:
                fullCommentInText = thisDiv[0].text
                stockSoup = thisDiv[0].find("a", {"class": "quote space"})
                if stockSoup is not None and stockSoup.has_attr('portfolioid') is not True:
                    stockName = stockSoup.get_text().replace("$", "")
                    stockUrl = stockSoup['href']
                commentSoup = thisDiv[0].find("span", class_=lambda x: x != 'title')
                if commentSoup is not None:
                    if commentSoup.a is not None:
                        commentSoup.a.decompose()
                        commentText = commentSoup.text
                        if len(commentText) > 0:
                            sentimentSnowNlp = snowNlpUtil.runSnowNlpSentiment(commentText)
                insertSnowNlpToDb(fid, uid, userName, stockName, stockUrl, commentText, fullCommentInText,
                                  uniqueId,
                                  sentimentSnowNlp)
                if index == len(listOfComments) - 1:
                    needToScrollDown = True
    return needToScrollDown


def scrapCommentInfiniteImpl():
    try:
        options, chrome_driver_path = webDriverUtil.webDriverSetup()
        driver = webdriver.Chrome(options=options, executable_path=chrome_driver_path)
        driver.get('https://q.futunn.com/hk/?lang=zh-hk')
        futuLogoCss = 'body > div.nn-pc.nn-hk > header > div > a > svg.nn-black-logo'
        while True:
            seleniumUtil.checkLoading(driver, futuLogoCss)
            soup = BeautifulSoup(driver.page_source, 'html.parser')
            listOfCommentsSoup = soup.select('#feedList > div > div')
            scrollDownFlag1 = scrapListOfSoupComment(listOfCommentsSoup)
            if scrollDownFlag1:
                webdriver.ActionChains(driver).send_keys(Keys.END).perform()
                seleniumUtil.checkLoading(driver, '#feedList > div > div:nth-child(20)')
                listOfCommentsSoup = soup.select('#feedList > div > div:nth-child(n+20)')
                scrollDownFlag2 = scrapListOfSoupComment(listOfCommentsSoup)
                if scrollDownFlag2:
                    webdriver.ActionChains(driver).send_keys(Keys.END).perform()
                    seleniumUtil.checkLoading(driver, '#feedList > div > div:nth-child(40)')
                    listOfCommentsSoup = soup.select('#feedList > div > div:nth-child(n+40)')
                    _ = scrapListOfSoupComment(listOfCommentsSoup)
            driver.refresh()
    except Exception as e:
        driver.quit()
        print(f'Retrying to handle error: ${e}')
        scrapCommentInfiniteImpl()


def scrapCommentInfinite():
    scrapCommentInfiniteImpl()
