import yfinance as yf
from django.db import connection

cur = connection.cursor()


def futuStockQuote():
    stockList = ["DWAC", "FUTU", "TSLA", "OCGN", "2800.HK", "PHUN", "3800.HK", "0700.HK", "AMC", "1810.HK", "GME",
                 "1024.HK", "0708.HK", "BKKT", "NIO", "LCID", "NVDA"]
    for stock in stockList:
        if ".HK" in stock:
            market = "HK"
        else:
            market = "US"
        msft = yf.Ticker(stock)
        histDf = msft.history(period="18d", interval="5m")
        for ind in histDf.index:
            thisDatetimeLocalized = str(ind.astimezone('Hongkong').to_pydatetime())
            uid = thisDatetimeLocalized + " | " + stock
            open = float(histDf['Open'][ind])
            high = float(histDf['High'][ind])
            low = float(histDf['Low'][ind])
            close = float(histDf['Close'][ind])
            volume = float(histDf['Volume'][ind])
            dividends = float(histDf['Dividends'][ind])
            stockSplits = float(histDf['Stock Splits'][ind])
            cur.execute(
                'insert into yfinance_stock_quote('
                '"uid",'
                '"timestamp",'
                '"stock",'
                '"market",'
                '"open",'
                '"high",'
                '"low",'
                '"close",'
                '"volume",'
                '"dividends",'
                '"stock_splits")'
                'VALUES ('
                '%s,'
                '%s,'
                '%s,'
                '%s,'
                '%s,'
                '%s,'
                '%s,'
                '%s,'
                '%s,'
                '%s,'
                '%s)',
                [uid, thisDatetimeLocalized, stock, market, open, high, low, close, volume, dividends, stockSplits])
    return
