from django.db import connection

from scrapFutu.utils import awsComprehendUtil, snowNlpUtil

cur = connection.cursor()


def patchCommentSentiment():
    try:
        cur.execute(
            "select * from [futu_data].[dbo].[community_comments] where cast([comment] as nvarchar(max)) != '' and sentiment IS NULL")
        resultList = cur.fetchall()
        for row in resultList:
            uniqueId = row[2]
            sentiment, sentimentScorePositive, sentimentScoreNegative, sentimentScoreNeutral, sentimentScoreMixed = awsComprehendUtil.runDetectSentiment(
                row[8])
            cur.execute(
                'update [futu_data].[dbo].[community_comments] '
                'set sentiment = %s,sentiment_score_positive = %s,sentiment_score_negative = %s,sentiment_score_neutral = %s,sentiment_score_mixed = %s '
                'where unique_id = %s',
                [sentiment, sentimentScorePositive, sentimentScoreNegative, sentimentScoreNeutral, sentimentScoreMixed,
                 uniqueId])
    except Exception as e:
        connection.close()
        raise e


def patchCommentSentimentSnowNlp():
    try:
        cur.execute(
            "select top(1000) unique_id, comment from [futu_data].[dbo].[community_comments] where cast([comment] as nvarchar(max)) != '' and sentiment_snownlp IS NULL")
        resultList = cur.fetchall()
        while len(resultList) > 0:
            cur.execute(
                "select count(*) from [futu_data].[dbo].[community_comments] where cast([comment] as nvarchar(max)) != '' and sentiment_snownlp IS NULL")
            count = cur.fetchall()
            print(f"{count[0][0]} left...")
            cur.execute(
                "select top(1000) unique_id, comment from [futu_data].[dbo].[community_comments] where cast([comment] as nvarchar(max)) != '' and sentiment_snownlp IS NULL")
            resultList = cur.fetchall()
            for row in resultList:
                uniqueId = row[0]
                snowNlpSentiment = str(snowNlpUtil.runSnowNlpSentiment(row[1]))
                cur.execute(
                    'update [futu_data].[dbo].[community_comments] set sentiment_snownlp = %s where unique_id = %s',
                    [snowNlpSentiment, uniqueId])
    except Exception as e:
        connection.close()
        raise e
