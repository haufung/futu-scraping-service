from snownlp import SnowNLP

def runSnowNlpSentiment(text):
    return SnowNLP(SnowNLP(text).han).sentiments
